from datetime import datetime, timedelta

import pytest
from main.app import create_app
from main.database.db_helper import db as _db
from main.models import Client, Parking, ClientParking


@pytest.fixture
def app():
    _app = create_app()
    _app.config["TESTING"] = True
    _app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

    with _app.app_context():
        _db.create_all()
        user = Client(
            name="name",
            surname="surname",
            credit_card="credit_card",
            car_number="car_number",
        )
        user_in_parking = Client(
            name="in_parking",
            surname="in_parking",
            credit_card="in_parking",
            car_number="in_parking",
        )
        user_out_parking = Client(
            name="second_parking",
            surname="second_parking",
            credit_card="second_parking",
            car_number="sec_parking",
        )
        user_without_card = Client(
            name="name_not_card", surname="surname_not_card", car_number="car_number"
        )
        parking = Parking(
            address="NeverLand street",
            opened=True,
            count_places=10,
            count_available_places=10,
        )
        client_parking = ClientParking(
            client_id=2, parking_id=1, time_in=datetime.now()
        )
        client_parking_out = ClientParking(
            client_id=3,
            parking_id=1,
            time_in=datetime.now(),
            time_out=datetime.now() + timedelta(days=1),
        )
        client_parking_out_without_card = ClientParking(
            client_id=4,
            parking_id=1,
            time_in=datetime.now(),
            time_out=datetime.now() + timedelta(days=1),
        )
        _db.session.add(user)
        _db.session.add(user_in_parking)
        _db.session.add(user_out_parking)
        _db.session.add(user_without_card)
        _db.session.add(parking)
        _db.session.commit()
        _db.session.add(client_parking)
        _db.session.add(client_parking_out)
        _db.session.add(client_parking_out_without_card)
        _db.session.commit()

        yield _app
        _db.session.close()
        _db.drop_all()


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


@pytest.fixture
def db(app):
    with app.app_context():
        yield _db
