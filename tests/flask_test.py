import json
import pytest


def test_math_route(client) -> None:
    resp = client.get("/test_route?number=8")
    data = json.loads(resp.data.decode())
    assert data == 64


@pytest.mark.parametrize("route", ["/test_route?number=8", "/clients", "/clients/1"])
def test_route_status(client, route):
    rv = client.get(route)
    assert rv.status_code == 200


def test_client(client) -> None:
    resp = client.get("/clients/1")
    assert resp.status_code == 200
    assert resp.json == {
        "id": 1,
        "name": "name",
        "surname": "surname",
        "credit_card": "credit_card",
        "car_number": "car_number",
    }


def test_create_client(client) -> None:
    test_data = {
        "name": "Mixam",
        "surname": "Ninebug",
        "credit_card": "ytrewq",
        "car_number": "654321",
    }
    json_dta = json.dumps(test_data)
    resp = client.post("/clients", data=json_dta, content_type="application/json")
    assert resp.status_code == 201


def test_create_parking(client) -> None:
    test_data = {
        "address": "NeverLand street",
        "opened": True,
        "count_places": 10,
        "count_available_places": 10,
    }
    json_data = json.dumps(test_data)
    resp = client.post("/parkings", data=json_data, content_type="application/json")
    assert resp.status_code == 201


@pytest.mark.client_parkings
def test_new_client_parking(client) -> None:
    test_data = {
        "client_id": 1,
        "parking_id": 1,
    }
    json_data = json.dumps(test_data)
    resp = client.post(
        "/client_parkings", data=json_data, content_type="application/json"
    )
    assert resp.status_code == 201


@pytest.mark.client_parkings
def test_new_client_without_card(client) -> None:
    test_data = {
        "client_id": 4,
        "parking_id": 1,
    }
    json_data = json.dumps(test_data)
    resp = client.delete(
        "/client_parkings", data=json_data, content_type="application/json"
    )
    assert resp.status_code == 400


@pytest.mark.client_parkings
def test_client_parking_exists(client) -> None:
    test_data = {
        "client_id": 2,
        "parking_id": 1,
    }
    json_data = json.dumps(test_data)
    resp = client.post(
        "/client_parkings", data=json_data, content_type="application/json"
    )
    assert resp.status_code == 400


@pytest.mark.client_parkings
def test_next_authorized_client_parking(client) -> None:
    test_data = {
        "client_id": 3,
        "parking_id": 1,
    }
    json_data = json.dumps(test_data)
    resp = client.post(
        "/client_parkings", data=json_data, content_type="application/json"
    )
    assert resp.status_code == 200


@pytest.mark.client_parkings
def test_invalid_client_data(client) -> None:
    test_data = {
        "client_id": 5,
        "parking_id": 1,
    }
    json_data = json.dumps(test_data)
    resp = client.post(
        "/client_parkings", data=json_data, content_type="application/json"
    )
    assert resp.status_code == 404


@pytest.mark.client_parkings
def test_invalid_parking_data(client) -> None:
    test_data = {
        "client_id": 2,
        "parking_id": 2,
    }
    json_data = json.dumps(test_data)
    resp = client.post(
        "/client_parkings", data=json_data, content_type="application/json"
    )
    assert resp.status_code == 404


@pytest.mark.client_parkings
def test_client_parking_out(client) -> None:
    test_data = {
        "client_id": 2,
        "parking_id": 1,
    }
    json_data = json.dumps(test_data)
    resp = client.delete(
        "/client_parkings", data=json_data, content_type="application/json"
    )
    assert resp.status_code == 200


@pytest.mark.client_parkings
def test_client_parking_out(client) -> None:
    test_data = {
        "client_id": 2,
        "parking_id": 1,
    }
    json_data = json.dumps(test_data)
    resp = client.delete(
        "/client_parkings", data=json_data, content_type="application/json"
    )
    assert resp.status_code == 200
