import factory
from factory.fuzzy import FuzzyChoice
import random
from main.database.db_helper import db
from main.models import Client, Parking, ClientParking


class ClientFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Client
        sqlalchemy_session = db.session

    name = factory.Faker("first_name")
    surname = factory.Faker("last_name")
    credit_card = FuzzyChoice(["1231444", None])
    car_number = factory.Faker("license_plate")


class ParkingFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Parking
        sqlalchemy_session = db.session

    address = factory.Faker("address")
    opened = FuzzyChoice([True, False])
    count_places = factory.Faker("random_int", min=20, max=50)
    count_available_places = factory.LazyAttribute(lambda o: random.randrange(10, 20))


class ClientParkingFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = ClientParking
        sqlalchemy_session = db.session

    client_id = factory.SubFactory(ClientFactory)
    parking_id = factory.SubFactory(ParkingFactory)
    time_in = factory.Faker("date_time")
