from .factories import ClientFactory, ParkingFactory
from main.models import Client, Parking


def test_create_client_with_factory(db):
    client_1 = ClientFactory()
    db.session.commit()
    assert client_1.id is not None
    assert len(db.session.query(Client).all()) == 5


def test_create_parking_with_factory(db):
    parking = ParkingFactory()
    db.session.commit()
    assert parking.id is not None
    assert len(db.session.query(Parking).all()) == 2
