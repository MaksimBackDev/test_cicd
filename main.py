from main.app import create_app
from main.app import db

if __name__ == "__main__":
    app = create_app()
    app.run()
