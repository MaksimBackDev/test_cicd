from flask import Blueprint, jsonify, request

from main.database.db_helper import db

from .crud import create_client_handler, find_client_by_id, get_all_clients

client_blueprint = Blueprint("clients", __name__)


@client_blueprint.route("/clients/<int:client_id>", methods=["GET", "POST"])
def clients_by_id_handler(client_id):
    result = find_client_by_id(session=db.session, client_id=client_id)
    if result:
        return jsonify(result.to_json()), 200
    else:
        message = {"message": f"Client with id = {client_id} not found."}
        return jsonify(message), 404


@client_blueprint.route("/clients", methods=["GET", "POST"])
def clients_handler():
    if request.method == "GET":
        return jsonify(get_all_clients(session=db.session)), 200
    client_data = request.json.values()
    create_client_handler(session=db.session, data=client_data)
    return "Clients info added.", 201
