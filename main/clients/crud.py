from typing import List, Optional

from main.models import Client


def get_all_clients(session) -> List:
    clients: List[Client] = session.query(Client).all()
    return [u.to_json() for u in clients]


def find_client_by_id(session, client_id) -> Optional[Client]:
    client: Client = session.query(Client).get(client_id)
    if client:
        return client
    return None


def create_client_handler(session, data):
    name, surname, credit_card, car_number = data
    for_new_client_data = {
        "name": name,
        "surname": surname,
        "credit_card": credit_card,
        "car_number": car_number,
    }
    new_client = Client(**for_new_client_data)
    session.add(new_client)
    session.commit()
