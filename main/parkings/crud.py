from typing import Optional

from main.models import Parking


def find_client_by_id(session, client_id) -> Optional[Parking]:
    client: Parking = session.query(Parking).get(client_id)
    if client:
        return client
    return None


def create_parking_handler(session, data):
    address, opened, count_places, count_available_places = data
    new_parking = Parking(
        address=address,
        opened=opened,
        count_places=count_places,
        count_available_places=count_available_places,
    )
    session.add(new_parking)
    session.commit()


def find_parking_by_id(session, parking_id) -> Optional[Parking]:
    parking: Parking = session.query(Parking).get(parking_id)
    if parking:
        return parking
    return None
