from flask import Blueprint, request

from main.database.db_helper import db

from .crud import create_parking_handler

parking_blueprint = Blueprint("parking", __name__)


@parking_blueprint.route("/parkings", methods=["GET", "POST"])
def clients_handler():
    parking_data = request.json.values()
    create_parking_handler(session=db.session, data=parking_data)
    return "", 201
