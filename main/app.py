from flask import Flask, jsonify, request

from main.client_parking.client_parking_views import client_parkings_blueprint
from main.clients.clients_views import client_blueprint
from main.database.db_helper import db
from main.parkings.parkings_views import parking_blueprint


def create_app():
    app = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///prod.db"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    db.init_app(app=app)
    app.register_blueprint(client_blueprint)
    app.register_blueprint(parking_blueprint)
    app.register_blueprint(client_parkings_blueprint)

    @app.before_request
    def before_request_func():
        db.create_all()

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        db.session.remove()

    @app.route("/test_route")
    def math_route():
        number = int(request.args.get("number", 0))
        result = number**2
        return jsonify(result)

    return app
