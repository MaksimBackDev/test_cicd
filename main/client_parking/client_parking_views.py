from flask import Blueprint, jsonify, request

from main.database.db_helper import db

from .crud import (
    check_client_parking,
    client_parkings_in,
    client_parkings_out,
    get_client_and_parking,
    reload_new_note,
)

client_parkings_blueprint = Blueprint("clientparkings", __name__)
sess = db.session


@client_parkings_blueprint.route("/client_parkings", methods=["POST"])
def client_parkings_in_handler():
    data = request.json
    client_id, parking_id = data["client_id"], data["parking_id"]

    client, parking = get_client_and_parking(
        session=sess, client_id=client_id, parking_id=parking_id
    )
    if not (client and parking):
        return jsonify({"massage": "Client or parking not founded."}), 404

    if not parking.opened:
        message = {"massage": f"Parking with id={parking_id} closed."}
        return jsonify(message), 400

    if parking.count_available_places < 1:
        return jsonify({"massage": "All parking spaces are occupied."}), 400

    note_about_parkings_client = check_client_parking(
        session=sess, client_id=client_id, parking_id=parking_id
    )

    if note_about_parkings_client:
        if note_about_parkings_client.time_out is None:
            return jsonify({"massage": "Client is already parked."}), 400
        reload_new_note(session=sess, note=note_about_parkings_client)
        return "Client-parking information upgraded", 200

    client_parkings_in(session=sess, client=client, parking=parking)
    message = {"massage": "New information about clients parking crated."}
    return jsonify(message), 201


@client_parkings_blueprint.route("/client_parkings", methods=["DELETE"])
def client_parkings_out_handler():
    client_id, parking_id = request.json.values()
    client, parking = get_client_and_parking(
        session=sess, client_id=client_id, parking_id=parking_id
    )
    for_client_parking_data = {
        "session": sess,
        "client_id": client_id,
        "parking_id": parking_id,
    }
    if check_client_parking(**for_client_parking_data):
        if client.credit_card:
            client_parkings_out(session=sess, client=client, parking=parking)
            return "Have a good trip!", 200
        message = {"massage": "Client not added credit_card_number."}
        return jsonify(message), 400
    return jsonify({"massage": "Client is not parked."}), 404
