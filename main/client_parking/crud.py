import datetime

from sqlalchemy import and_

from main.clients.crud import find_client_by_id
from main.models import Client, ClientParking, Parking
from main.parkings.crud import find_parking_by_id


def client_parkings_in(session, client: Client, parking: Parking) -> None:
    client_parking: ClientParking = ClientParking(
        client_id=client.id,
        parking_id=parking.id,
        time_in=datetime.datetime.now(),
        time_out=None,
    )
    parking.count_available_places -= 1
    session.add(client_parking)
    session.commit()


def reload_new_note(session, note: ClientParking):
    note.time_in = datetime.datetime.now()
    note.time_out = None
    session.commit()


def client_parkings_out(session, client: Client, parking: Parking) -> None:
    id = client.id
    id2 = parking.id
    client_parking = (
        session.query(ClientParking)
        .filter(
            and_(
                ClientParking.client_id == id,
                ClientParking.parking_id == id2,
                ClientParking.time_out == None,  # noqa: E711
            )
        )
        .first()
    )

    client_parking.time_out = datetime.datetime.now()
    parking.count_available_places += 1
    session.commit()


def get_client_and_parking(session, client_id, parking_id):
    client = find_client_by_id(session=session, client_id=client_id)
    parking = find_parking_by_id(session=session, parking_id=parking_id)
    return client, parking


def check_client_parking(session, client_id, parking_id):
    client_parking = (
        session.query(ClientParking)
        .filter(ClientParking.client_id == client_id)
        .filter(ClientParking.parking_id == parking_id)
        .first()
    )
    if client_parking:
        return client_parking
    else:
        return None
